<?php
/**
 *  Auto load all the core basis of Classes and Races.
 *  Auto load all the classes.
 *  Auto load all the races.
 */
function autoload($className){
    //list comma separated directory name
    $directory = array('', 'core/classes/', 'game/game_classes/classes/', 'game/game_races/classes/');

    //list of comma separated file format
    $fileFormat = array('%s.php', '%s.class.php');

    foreach ($directory as $current_dir){
        foreach ($fileFormat as $current_format){

            $path = $current_dir.sprintf($current_format, $className);
            if (file_exists($path)){
                include $path;
                return;
            }
        }
    }
}

spl_autoload_register('autoload');

