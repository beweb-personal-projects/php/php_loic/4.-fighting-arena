# [ **FIGHTER'S RIFT** ] #

- **Fighter's Rift** is an arena-based game, the game consists in turns until one player is left alive. The game has multiple features, such as Classes, Races, etc. The game has configuration files to facilitate changing of stats, etc.


## [ **DOM** ] ##

- DOM represents the arena itself.
- Management and creation of rounds **until one player is left alive**.
- Ajustements of the player's health to every player added to the arena, so we can maintain the integrity of the game.
- At the beginning of every game the players are suffled so that the same player is not **repeated**.
- Every detail of the Round is sent to de website as simple as possible.
- At the end of the game we will notify the winner of the game.


## [ **FIGHTER** ] ##

- The Fighter is literally the representation of the player's character, so it will have some properties to define the player itself, such as name, race, and class. The fighter will to calculate the differences of stats between race and class, so we have a precise calculation of both of them.

## [ **CLASSES** ] ##

- All classes have specific stats such as (Health, Damage and Critical)


```
    // Implemented Classes

    - Druid
    - Warrior
    - Rogue
```

## [ **RACES** ] ##

- All races have specific stats multipliers that will increase the base stats of the classes.

```
    // Implemented Races

    - Elf
    - Human
    - Orc
```

## [ **INSTALLATION** ] ##

- You just need to download game and run it directly, the main execution file is index.php


## [ **PERSONAL INFO** ] ##

- If you guys have any questions u can add my on discord: stracKz#3778
