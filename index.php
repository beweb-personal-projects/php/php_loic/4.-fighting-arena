<?php
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));
require(ROOT . 'init.php');

//-------------------------------------------------------------------------------------
// [ STARTING THE GAME ]

$dom = new Dom();

$dom->add([
    new Fighter("strackz", new Druid(), new Human()),
    new Fighter("Nelle", new Rogue(), new Elf()),
    new Fighter("Loic", new Warrior(), new Orc()),
]);

$dom->start();
