<?php

//--------------------------------------------------------------------------------------------
// [ DRUID ]

trait Druid_Config {
    private $class_config = [
        "health" => 450.0,
        "damage" => 25.0,
        "critical" => 35.0,
        "color" => "#FF7C0A",
        "armor-type" => "Leather"
    ];
}

//--------------------------------------------------------------------------------------------
// [ ROGUE ]

trait Rogue_Config {
    private $class_config = [
        "health" => 300.0,
        "damage" => 40.0,
        "critical" => 50.0,
        "color" => "#FFF468",
        "armor-type" => "Leather"
    ];
}

//--------------------------------------------------------------------------------------------
// [ WARRIOR ]

trait Warrior_Config {
    private $class_config = [
        "health" => 500.0,
        "damage" => 15.0,
        "critical" => 10.0,
        "color" => "#C69B6D",
        "armor-type" => "Plate"
    ];
}