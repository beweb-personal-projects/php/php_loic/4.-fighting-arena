<?php
// Require Config before initializing the classes
include_once(ROOT."game/game_classes/classes_config.php");

class Rogue extends Classes {
    use Rogue_Config;

    public function __construct(){
        $this->health = $this->class_config['health'];
        $this->damage = $this->class_config['damage'];
        $this->critical = $this->class_config['critical'];
        $this->color = $this->class_config['color'];
        $this->armor_type = $this->class_config['armor-type'];
    }
}
