<?php

//--------------------------------------------------------------------------------------------
// [ ELF ]

trait Elf_Config {
    private $race_config = [
        "multiplier_health" => 1.2,
        "multiplier_damage" => 1.0,
        "multiplier_critical" => 1.5,
    ];
}

//--------------------------------------------------------------------------------------------
// [ HUMAN ]

trait Human_Config {
    private $race_config = [
        "multiplier_health" => 1.0,
        "multiplier_damage" => 1.5,
        "multiplier_critical" => 1.2,
    ];
}

//--------------------------------------------------------------------------------------------
// [ ORC ]

trait Orc_Config {
    private $race_config = [
        "multiplier_health" => 1.5,
        "multiplier_damage" => 1.2,
        "multiplier_critical" => 1.0,
    ];
}