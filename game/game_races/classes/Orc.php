<?php
// Require Config before initializing the classes
include_once(ROOT."game/game_races/races_config.php");

class Orc extends Races {
    use Orc_Config;

    public function __construct(){
        $this->multiplier_health = $this->race_config['multiplier_health'];
        $this->multiplier_damage = $this->race_config['multiplier_damage'];
        $this->multiplier_critical = $this->race_config['multiplier_critical'];
    }
}