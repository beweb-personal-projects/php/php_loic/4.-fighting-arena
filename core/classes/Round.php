<?php 
require(ROOT. 'core/interfaces/interface_round.php');

class Round extends Dom implements interface_round{
    private static array $all_rounds = [];
    private static object $winner;

    public function __construct(){
        array_push(self::$all_rounds, $this);
    }

    //--------------------------------------------------------------------------------------------
    // GETTERS

    public function getRoundCounter():int{
        return count(self::$all_rounds);
    }

    public static function getWinner(): Fighter {
        return self::$winner;
    }

    //--------------------------------------------------------------------------------------------
    // SETTERS

    public function turn():void {
        $this->viewRoundCounter();

        // First Loop is for the player that is attacking
        foreach (parent::$all_ground_fighters as $i => $fighter) {
            // In the first loop iteration we initialize the winner
            if(!isset(self::$winner))
                self::$winner = $fighter;

            //Verify at the beginning of a new attacking player if he's still alive
            if($fighter->getFinalStats()['health'] > 0){
                // Second Loop is for the player that is Receiving the Damage
                foreach (parent::$all_ground_fighters as $j => $defending) {
                    if($defending->getFinalStats()['health'] > 0){
                        // The fighter can atack all the other players, but he can't attack himself
                        if($fighter !== $defending) {
                            $defending->removeHealth($fighter->getCurrentDamage());
                            $this->viewLog($fighter, $defending);
                        }
                    } else {
                        // If player (Defender) has 0 health will be removed directly from the arena
                        $this->removeFighter($defending);
                    }

                    // It ajusts the winner if the fighter has lower heath than the defender
                    if($fighter->getFinalStats()['health'] < $defending->getFinalStats()['health']) 
                        self::$winner = $defending;
                }
            } else {
                // If player (Attacker) has 0 health will be removed directly from the arena
                $this->removeFighter($fighter);
            }

            // It ajusts the winner if the fighter has higher heath than the current winner's health
            if($fighter->getFinalStats()['health'] > self::$winner->getFinalStats()['health']) 
                self::$winner = $fighter;
        }
    }

    public function removeFighter(Fighter $remove_fighter):void {
        // Removes the player from the arena and sends a notification of death.
        foreach (parent::$all_ground_fighters as $i => $fighter) {
            if ($fighter === $remove_fighter) {
                unset(parent::$all_ground_fighters[$i]); // Remove from the array
                parent::$all_ground_fighters = array_values(parent::$all_ground_fighters); // Re-ajust the Indexes of the Array
            }
        }
        $this->viewDeathNotification($remove_fighter->getName());
    }

    //--------------------------------------------------------------------------------------------
    // VIEW

    public function viewRoundCounter():void {
        echo "<h2>Round " . $this->getRoundCounter() . "</h2>";
    }

    public function viewDeathNotification(string $fighter):void {
        echo " <p>You just Died: <b>" . $fighter . "</b></p>";
    }

    public function viewLog(Fighter $fighter, Fighter $defending):void {
        echo "<p style='background-color:". $fighter->getClass()->getColor() .";font-weight:bold; padding: 5px;'>Attacker: " . $fighter->getName() . " (HP: " . $fighter->getFinalStats()['health'] .") -->|DPS: ". $fighter->getCurrentDamage() ."|--> Defender: " . $defending->getName() . " (HP: " . $defending->getFinalStats()['health'] . ")</p>";
    }
}
