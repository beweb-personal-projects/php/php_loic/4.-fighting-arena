<?php
require(ROOT. 'core/interfaces/interface_fighter.php');

class Fighter implements interface_fighter{
    private string $name;
    private array $fighter_stats;
    private Classes $class;
    private Races $race;

    public function __construct(string $c_name, Classes $c_class, Races $c_race){
        $this->name = $c_name;
        $this->class = $c_class;
        $this->race = $c_race;

        $this->fighter_stats = [
            "health" => $this->getFinalHealth(),
            "damage" => $this->getFinalDamage(),
            "critical" => $this->getFinalCritical(),
        ];
    }

    //--------------------------------------------------------------------------------------------
    // GETTERS

    public function getName():string {
        return $this->name;
    }

    public function getFinalHealth():float {
        return $this->class->getHealth() * $this->race->getMultiplierHealth();
    }

    public function getFinalDamage():float {
        return $this->class->getDamage() * $this->race->getMultiplierDamage();
    }

    public function getFinalCritical():float {
        return $this->class->getCritical() * $this->race->getMultiplierCritical();
    }

    public function getFinalStats():array {
        return $this->fighter_stats;
    }

    public function getClass():object {
        return $this->class;
    }

    public function getRace():object {
        return $this->race;
    }

    public function isCritical(): bool {
        // If the player's critical chance is equal or bigger than the random, the player will critical strike
        if(random_int(1,100) <= $this->getFinalCritical()) 
            return true;
        else 
           return false;
    }

    public function criticalBonus(): float {
        return random_int(1,30);
    }

    public function getCurrentDamage():float {
        // If the player crits we will sum the base damage of the player to a critical bonus (Random Value)
        if($this->isCritical()) 
            return $this->getFinalDamage() + $this->criticalBonus();
        else 
            return $this->getFinalDamage();
    }

    //--------------------------------------------------------------------------------------------
    // SETTERS

    public function removeHealth(float $received_damage):void {
        if($this->fighter_stats['health'] > $received_damage)
            $this->fighter_stats['health'] -= $received_damage;
        else
            $this->fighter_stats['health'] = 0;
    }

    public function ajustHealth(float $health):void {
        $this->fighter_stats['health'] = $health;
    }
}