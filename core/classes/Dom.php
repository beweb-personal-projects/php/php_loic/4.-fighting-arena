<?php
require(ROOT. 'core/interfaces/interface_dom.php');

class Dom implements interface_dom{
    protected const ARENA_NAME = "Fighter's Rift";
    protected static array $all_ground_fighters = [];

    public function __construct(){}

    //--------------------------------------------------------------------------------------------
    // GETTERS

    public static function getGroundFighters():array {
        return self::$all_ground_fighters;
    }

    //--------------------------------------------------------------------------------------------
    // SETTERS

    public function add(array $fighters):void {
        foreach ($fighters as $fighter) {
            array_push(self::$all_ground_fighters, $fighter);
        }
        $this->shuffleFighters();
    }
    
    public function shuffleFighters():void {
        shuffle(self::$all_ground_fighters);
    }
    
    public function balanceHealth():void {
        if(count(self::$all_ground_fighters) > 2) {
            $extra_players = count(self::$all_ground_fighters) - 2;

            // Reajusts the health for every extra player in the arena
            for ($i=0; $i < $extra_players; $i++) { 
                // Changes the health to every player in the arena
                foreach (self::$all_ground_fighters as $fighter) {
                    $fighter->ajustHealth($fighter->getFinalStats()['health'] + ($fighter->getFinalHealth() / 2));
                }
            }
            $this->viewExtraPlayers($extra_players);
        }
    }


    public function start():void {
        $this->viewWelcomeArena();

        $this->balanceHealth();

        while(count(self::$all_ground_fighters) > 1) {

            //Change player's position in the array at every beginning of a new round
            $this->shuffleFighters();

            //Start Round
            $current_round = new Round();
            $current_round->turn();
        }

        $this->viewWinner(strtoupper(Round::getWinner()->getName()));
    }

    //--------------------------------------------------------------------------------------------
    // VIEWS

    public function viewWelcomeArena():void {
        echo "<h1>Welcome to the Arena - " . self::ARENA_NAME . "</h1>";
    }

    public function viewWinner(string $winner):void {
        echo "<h1>THE WINNER IS: " . $winner . "</h1>";
    }

    public function viewExtraPlayers(int $extra_players):void {
        echo "<h3>Players Health Ajusted to Extra Players: " . $extra_players . "</h3>";
    }
}