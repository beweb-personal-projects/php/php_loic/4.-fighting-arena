<?php
require(ROOT. 'core/interfaces/interface_races.php');

abstract class Races implements interface_races {
    protected float $multiplier_health;
    protected float $multiplier_damage;
    protected float $multiplier_critical;

    //--------------------------------------------------------------------------------------------
    // GETTERS

    public function getMultiplierHealth():float {
        return $this->multiplier_health;
    }

    public function getMultiplierDamage():float {
        return $this->multiplier_damage;
    }

    public function getMultiplierCritical():float {
        return $this->multiplier_critical;
    }
}
