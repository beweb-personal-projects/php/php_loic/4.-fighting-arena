<?php
require(ROOT. 'core/interfaces/interface_classes.php');


abstract class Classes implements interface_classes {
    protected float $health;
    protected float $damage;
    protected float $critical;
    protected string $color;
    protected string $armor_type;
    
    //--------------------------------------------------------------------------------------------
    // GETTERS
    
    public function getHealth():float {
        return $this->health;
    }
    
    public function getDamage():float {
        return $this->damage;
    }
    
    public function getCritical():float {
        return $this->critical;
    }
    
    public function getColor():string {
        return $this->color;
    }

    public function getArmorType():string {
        return $this->armor_type;
    }
}
