<?php

interface interface_fighter {
    //Calculation between Class and Race
    public function getFinalHealth():float;
    public function getFinalDamage():float;
    public function getFinalCritical():float;
    public function getFinalStats():array;

    //Stats Manipulation
    public function isCritical(): bool;
    public function criticalBonus(): float;
    public function getCurrentDamage():float;
    public function removeHealth(float $received_damage):void;
    public function ajustHealth(float $health):void;

    //Others
    public function getName():string;
    public function getClass():object;
    public function getRace():object;
}