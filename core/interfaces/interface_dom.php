<?php

interface interface_dom {
    // Others
    public static function getGroundFighters():array;
    public function add(array $fighters):void;
    public function shuffleFighters():void;
    public function start():void;
    public function balanceHealth():void;

    // Views
    public function viewWelcomeArena():void;
    public function viewWinner(string $winner):void;
    public function viewExtraPlayers(int $extra_players):void;
}