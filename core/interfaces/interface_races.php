<?php

interface interface_races {
    //Stat Multipliers
    public function getMultiplierHealth():float;
    public function getMultiplierDamage():float;
    public function getMultiplierCritical():float;
}