<?php

interface interface_classes {
    // Class Stats
    public function getHealth():float;
    public function getDamage():float;
    public function getCritical():float;

    // Class Details
    public function getColor():string;
    public function getArmorType():string;
}