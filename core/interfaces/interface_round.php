<?php

interface interface_round {
    // Others
    public function getRoundCounter():int;
    public static function getWinner(): Fighter;
    public function turn():void;
    public function removeFighter(Fighter $remove_fighter):void;

    // Views
    public function viewRoundCounter():void;
    public function viewDeathNotification(string $fighter):void;
    public function viewLog(Fighter $fighter, Fighter $defending):void;
}